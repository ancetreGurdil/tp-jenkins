package fr.supdevinci.m1.cicd.tpcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpcicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpcicdApplication.class, args);
	}

}
